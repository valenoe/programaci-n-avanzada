#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from cuenta import Cuenta
import random

'''
Escribe una clase Cuenta para representar una cuenta bancaria. Los datos de la cuenta son:
nombre del cliente
número de cuenta
saldo
tipo de interés (float)
La clase debe contender los siguientes métodos:
Constructor
Métodos Set/Get para asignar y obtener los datos de la cuenta (atributos privados).
Métodos ingreso que aumente el saldo en la cantidad que se indique. Se debe validar que esa
cantidad no puede ser negativa.
Método giro para disminuir el saldo en una cantidad, pero antes se debe comprobar que hay saldo
suficiente. La cantidad no puede ser negativa.
Como condiciones se pide que:
Los métodos ingreso y giro devuelvan True si la operación se ha realizado con éxito o False en
caso contrario.
Método transferencia debiese permitir pasar dinero de una cuenta a otra siempre que en la cuenta
de origen haya dinero suficiente.
Por ejemplo: método transferencia: cuentaOrigen.transferencia(cuentaDestino, importe); que indica
que queremos hacer una transferencia desde cuentaOrigen a cuentaDestino del importe indicado.
Para este ejercicio puede establecer datos predefinidos como el saldo y la cuenta para realizar pruebas
de conceptos y validaciones.

'''
def nombre_():
    '''
    Esta función pide el nombre del tirular de la cuenta
    '''
    nombre = str(input("Ingrese el nombre del titular de la cuenta: "))
    return nombre

def n_cuenta():
    '''
    Genera un número random para asignarlo como número de cuenta
    '''
    numero = random.randint(12345678, 98765432)
    print("su numero de cuenta es: ", numero)
    return numero

def saldo_inicial_1():
    '''
    Genera un número random para asignarlo como saldo inicial
    '''
    saldo = random.randint(0,100000)
    print("su saldo es {0}".format(saldo))
    return saldo

def saldo_(saldo_inicial):
    '''
    Esta función empieza con un menú, pero ambas opciones 
    asignan valores a las variables que retornan,
    estas luego se va a sumar o restar al sueldo
    x guarda los valores que suman,
    y, los valores que restan
    '''
    
    print("opciones:\n1 para aumentar saldo\n2 para hacer un giro")
    option = int(input("--> "))
    if option == 1:
        x = int(input("Cuanto va a agregar: "))
        y = 0
    elif option == 2:
        y = int(input("Cuanto va a girar: "))
        x = 0
    else:
        print("***** Esto no esta en las opciones *****")
        x = 0
        y = 0
    return x, y

def transferencia(original, nueva):
    '''
    Cuando ya esté creada la cuenta y haya hecho las disminuciones 
    0o aumentos necesarios, se pedirá que transfiera dinero a otra cuneta,
    esta función preguntará cunato desea transferir.
    La función tambien examina que sean numeros positivos y
    que en la cuenta haya saldo suficiente
    retorna lo que queda en la primera y segnda cuenta después de transferir 

    '''
    x = int(input("Cuanto piemsa transferir: "))
    if x > 0: 
        if x <= original:
            saldo_restante = original - x
            saldo_nueva = nueva + x
        else: 
            print("No tiene saldo suficiente")
    else:
        print("no escriba numeros negativos ni cero")
        transferencia(original, nueva)

    return saldo_restante, saldo_nueva 

if __name__ == "__main__":
    cuenta_original = Cuenta()
    cuenta_original.set_nombre(nombre_())
    cuenta_original.set_numero_cuenta(n_cuenta())
    
    
    cuenta_original.set_saldo(saldo_inicial_1())
    mas, menos = saldo_(cuenta_original.get_saldo())
    cuenta_original.set_giro_(menos)
    cuenta_original.set_aumento(mas)

    print("\ndebe transferir dinero a otra cuenta")
    cuenta_nueva = Cuenta()
    cuenta_nueva.set_nombre(nombre_())
    cuenta_nueva.set_numero_cuenta(n_cuenta())
    
    
    cuenta_nueva.set_saldo(saldo_inicial_1())
    original, nueva = transferencia(cuenta_original.get_saldo(), cuenta_nueva.get_saldo())
    cuenta_original.set_saldo(original)
    cuenta_nueva.set_saldo(nueva)


    print("su saldo es: ", cuenta_original.get_saldo())
    print("el saldo de la otra cuenta es: ", cuenta_nueva.get_saldo())
    