#!/usr/bin/env python3
# -*- coding:utf-8 -*-

class Cuenta():

    def __init__(self):
        self.__nombre = None
        self.__numero_cuenta = None
        self.__saldo = None
        #self.__interes = None

    def set_nombre(self, nombre):
        self.__nombre = nombre

    def get_nombre(self):
        return self.__nombre

    def set_numero_cuenta(self, numero):
        self.__numero_cuenta = numero

    def get_numero_cuenta(self):
        return self.__numero_cuenta

    def set_saldo(self, saldo):
        self.__saldo = saldo

    def get_saldo(self):
        return self.__saldo
            
    def set_aumento(self, mas):
        if isinstance(mas, int):
            if mas >= 0:
                self.__saldo = self.__saldo + mas
                print("usted ha aumentado {0} pesos".format(mas))
            else:
                print("Ocupe otra funcion")
    def get_aumento(self):
        return self.__saldo
    
    def set_giro_(self, giro):
        if giro >= 0:
            if self.__saldo >= giro:
                self.__saldo = self.__saldo - giro
                print("usted ha girado {0} pesos".format(giro))

        else:
            print("No ponga numeros negativos")
        
    def get_giro_(self):
        return self.__saldo

    
    