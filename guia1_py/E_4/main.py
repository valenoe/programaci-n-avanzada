#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from amigo import Amigo
import random

'''
Pepe salió a celebrar su santo con un grupo de amigos y decidieron ir a comer a un restaurante. Pepe
y sus amigos acostumbran pagar la cuenta por partes iguales, es decir, cada uno paga lo mismo. Sin
embargo, la cuenta incluye el consumo de todos los comensales, y no considera el IVA (19 %) ni la
propina (10 % ). El problema es que no saben cuánto debe pagar cada uno, por lo que usted debe
entregar una solución.
Para este ejercicio se pide cree una clase que represente a los amigos (atributos y métodos), y que
existirá un atributo privado llamado disponible que representará el dinero actual del objeto. Si este no
logra cubrir la división de la cuenta, otro objeto (un amigo o amigos) deberá (án) cubrir lo que falta
para pagar el total al restaurante.
'''
def nombre():
    name = str(input("Como se llama: "))
    return name

def cuenta_restaurante():
    '''
    esta función pregunta cuanto salió la cuenta
    y la divide por 4 partes iguales
    '''
    cuenta = int(input("Cuanto salio la cuenta: "))
    cada_uno = cuenta / 4
    print("cada uno debe pagar {0} pesos".format(cada_uno))
    return cada_uno

def definir_dinero_disponible():
    '''
    define cuanto dinero tiene cada amigo,
    entre 0 y 1000
    '''
    dinero = random.randint(0,1000)
    print("usted tiene: ", dinero)
    return dinero

def si_no_puede_pagar(a, b, c, d, cuenta):
    '''
    entra a la función si el que se está comparando en el if
    tiene menos dinero de lo que debe pagar cada uno
    Para poder pagar a cuenta se le suma el dinero
    del amigo siguiente en la lista al momento de llamar la función
                     a  = a + b
    ej:   dinero "pepe" = dinero "pepe" + dinero amigo2 
    a1 es lo que sobra despues de pagar la cuenta de ambos, 
    si con eso no basta, se suma el dinero del tercer amigo
    y así sucesivamente. 
    si alcanza, la función va a retornar un número
    que en otra función se le va a dar significado

    '''
    a = a + b
    a1 = a - cuenta * 2
    if a1 < 0:
        a = a + c
        a2 = a - cuenta * 3
        if a2 < 0:
            a = a + d
            a3 = a - cuenta * 4
            if a3 < 0:
                return 5
            else:
                return 4
        else:
            return 3
    else:
        return 2

def resultado_de_pago(n):
    '''
    Esta función le da significado al número que retorna a n

    '''
    if n == 1:
        print("pudo pagar cada uno su parte")
    elif n == 2:
        print("un amigo ayudo a pagar a otro")
    elif n == 3:
        print("un amigo ayudo a pagar a 2 que no podian")
    elif n == 4:
        print("un amigo ayudo a pagar a 3 que no podian")
    else:
        print("no pudieron pagar la cuenta entre los 4")
    

def poder_pagar(dd_1, dd_2, dd_3, dd_4, cada_uno):
    '''
    dd significa dinero disponible, el numero identifca a cada amigo
    cada_uno = 1/4 de la cuenta, lo que deberá pagar cada uno
    Esta función identifica si el dinero disponible de cada uno de los amigos
    alcanza para pagar su parte de la cuenta, si no les alcanza 
    los lleva a una función que, sumando sus saldos, verifica si pueden pagar
    esta retorna un numero que sirve como código para identificar como pagaron la cuenta 
    '''
    if dd_1 < cada_uno:
        n = si_no_puede_pagar(dd_1, dd_2, dd_3, dd_4, cada_uno)
    elif dd_2 < cada_uno:
        n = si_no_puede_pagar(dd_2, dd_1, dd_3, dd_4, cada_uno)
    elif dd_3 < cada_uno:
        n = si_no_puede_pagar(dd_3, dd_2, dd_1, dd_4, cada_uno)
    elif dd_4 < cada_uno:
        n = si_no_puede_pagar(dd_4, dd_2, dd_3, dd_1, cada_uno)
    else:
        n = 1
    resultado_de_pago(n)

try:
    if __name__ == "__main__":
        amigo1 = Amigo(nombre())
        amigo1.set_disponible(definir_dinero_disponible())
        amigo2 = Amigo(nombre())
        amigo2.set_disponible(definir_dinero_disponible())
        amigo3 = Amigo(nombre())
        amigo3.set_disponible(definir_dinero_disponible())
        amigo4 = Amigo(nombre())
        amigo4.set_disponible(definir_dinero_disponible())

    
        cuenta_personal = cuenta_restaurante()

        poder_pagar(amigo1.get_disponible(), amigo2.get_disponible(), amigo3.get_disponible(), amigo4.get_disponible(), cuenta_personal)
  

except:
    print("no puso numeros en la cuenta")


