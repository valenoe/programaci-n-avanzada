#!/usr/bin/env python3
# -*- coding:utf-8 -*-

class Amigo():

    def __init__(self, nombre):
        self.nombre = nombre
        self.__disponible = None

    def set_disponible(self, dinero):
        self.__disponible = dinero

    def get_disponible(self):
        return self.__disponible
  