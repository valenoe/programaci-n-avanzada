#!/usr/bin/env python3
# -*- coding:utf-8 -*-

class Perro():

    def __init__(self, ama):
        self.__color = None
        self.__raza = None
        self.__sexo = None
        self.edad = None
        self.__peso = None
        self.__nombre = None
        self.nombre_persona_responsable = ama

    def set_color(self, color):
        self.__color = color

    def get_color(self):
        return self.__color

    def set_raza(self, raza):
        self.__raza = raza

    def get_raza(self):
        return self.__raza

    def set_sexo(self, sexo):
        self.__sexo = sexo.upper()

    def get_sexo(self):
        return self.__sexo

    def get_edad(self, edad):
        self.edad = edad * 12
        return self.edad

    def set_peso(self, peso):
        self.__peso = peso

    def get_peso(self):
        return self.__peso

    def set_nombre(self, nombre):
        self.__nombre = nombre

    def get_nombre(self):
        return self.__nombre

    def ladrar1(self, ladra):
        self.ladrar = ladra
        return self.ladrar

    def jugar1(self, jugar):
        self.jugar = jugar
        return self.jugar

    def comer1(self, comer):
        self.comer = comer
        return self.comer

    def territorio(self):
        if self.__sexo == "HEMBRA":
            self.ser_territorial = "No"
        elif self.__sexo == "MACHO":
            self.ser_territorial = "Si"
        return self.ser_territorial

    def imprimir(self):
        #return(self.nombre + "pesa " + self.peso + "kg")
        print("\n{0} es responsable de {1}".format(self.nombre_persona_responsable, self.__nombre))
        print("{0} tiene {1} meses y pesa {2} kg".format(self.__nombre, 
                                                       self.edad, 
                                                       self.__peso))
        print("es {0}, {2} de color {1}".format(self.__sexo,
                                                self.__color,
                                                self.__raza))
