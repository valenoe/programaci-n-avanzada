#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from perro import Perro
import random
'''
Crear una clase llamada Perro, con los atributos y acciones que tienen los perros (son de un color,
pertenecen a una raza, tienen una edad, un determinado sexo - son machos o hembras - y tienen un
peso que se puede expresar en kilogramos). Sobre las acciones podemos imaginar que ellos ladran,
juegan, comen y si son machos se pelean entre ellos para disputarse el terreno. Desde la clase principal
deberá crear una instancia a la clase Perro (objeto) y darle vida, complementando y extendiendo los
datos de los atributos y las acciones ejemplificadas.
'''
def bienvenida():
    print("Hola, bienvenido a 'Padres perrunos'")
    nombre = str(input("Por favor danos el nombre de la persona que se va a hacer responsable: "))
    print("\nOK {0}, sigue los pasos para elegir al amor de tu vida".format(nombre))
    return nombre

def raza():
    '''
    Esta función muestra las razas y colores de perros que han sido rescatados
    se establece su edad y su peso
    se muestra un listado de razas con su respectivo código para poder escgoger una

    '''
    print("\nHemos encontrado perros callejeros de las siuientes razas")
    razas = {"1": "Quiltro", "2": "Pastor Aleman", "3": "Husky Siberiano", "4": "Bulldog", "5": "Shnivba inu", "6": "Chihuahua", "7": "Bbeagle", "8": "Boxer", "9": "Chow Chow", "10": "Pastor Escoces de pelo largo"}
    for key, value in razas.items():
        print(key, ":", value)
    raza = str(input("escriba el numero de la raza que le interesa: "))
    return razas[raza]

def color(raza):
    '''
    Esta función hace lo mismo que la función raza, pero con los colores
    de cada uno de los perros encontrados
    '''
    print("Para esta raza exinten los giguientes colores: ")
    colores = {"Quiltro": {1: "Negro", 2: "Cafe", 3: "Blanco", 4: "Tricolor"}, "Pastor Aleman": {1: "Cafe con negro"}, "Husky Siberiano":{1: "Blanco", 2: "Blanco con plomo", 3: "Blanco con cafe"}, "Bulldog": {1: "Cafe con pecho blanco"}, "Shnivba inu":{1: "Cafe anaranjado"}, "Chihuahua":{1: "Cafe", 2: "Negro"}, "Bbeagle":{1: "Cafe con lomo negro y pecho blanco"}, "Boxer":{1: "Cafe"}, "Chow Chow":{1: "Negro", 2:"Cafe oscuro", 3: "Cafe claro"}, "Pastor Escoces de pelo largo": {1: "Arena y blanco", 2:"Tricolor"}}
    for key, values in colores.items():
        if key == raza:
            for llave, valor in colores[key].items():
                print(llave, ":", valor)
            
            x = int(input("escriba el numero del color que le interesa: "))
            color = values[x]
    return color

def datos(raza):
    '''
    esta función determina el peso, el sexo y la edad del perrito que se escoge
    para eso asigna números aleatorios a peso y edad 
    se toma n consideración el porte para asignarle un peso
    '''
    if raza == "Chihuahua":
        n2 = 10
        n1 = 1
    elif raza == "Bbeagle":
        n1 = 2
        n2 = 20
    else:
        n2 = 30
        n1 = 10
    kg = random.randint(n1, n2)

    s = ["HEMBRA", "MACHO"]
    sexo = s[random.randint(0,1)]
    
    edad = random.randint(0, 18)

    print("Tenemos a un perro de {0} kg con esa descripcion, es {1} y tiene {2} vueltas al sol".format(kg, sexo, edad))
    return kg, sexo, edad

def adopcion():
    nombre = str(input("Dele un nombre a su mascota: "))
    return nombre
    
def opcion():
    '''
    Se crea un ramdom en una función a parte para que en la 
    función parque de resultados diferentes
    '''
    x = random.randint(0, 1)
    return x

def parque(mi_perro):
    '''
    Según los resultados de random en la función opcion
    el perro ladra, juega o vuelve para comer
    También imprime si es territorial segú el sexo
    '''

    x = opcion()
    if x == 1:
        mi_perro.ladrar1("guau - guau")
    else:
        mi_perro.ladrar1("   \n(silencio)")

    y = opcion()
    if y == 0:
        mi_perro.jugar1("Tu perro se lleva bien con los demas")
    else:
        mi_perro.jugar1("Tu perro es timido")

    print("{0}\n{1}".format(mi_perro.ladrar, mi_perro.jugar))
    print("tu perro {0} es territorial".format(mi_perro.territorio()))

    z = opcion()
    print("\nFiiiiiiii\n(silvido)")
    if z == 1:
        mi_perro.comer1("Nos vamos a casa a comer")
    else:
        mi_perro.comer1("No te escucho, ve a buscralo")
    print(mi_perro.comer)



try:

    if __name__ == "__main__":
        mi_perro = Perro(bienvenida())
        mi_perro.set_raza(raza())
        mi_perro.set_color(color(mi_perro.get_raza()))
        peso, sexo, edad = datos(mi_perro.get_raza())
        mi_perro.set_peso(peso)
        mi_perro.set_sexo(sexo)
        mi_perro.get_edad(edad)
        mi_perro.set_nombre(adopcion())
        print("\n\n----PARQUE DE PERROS----\n\n")
        parque(mi_perro)
        mi_perro.imprimir()

except:
    print("Esto no esta en el rango")