#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import random
from trajeta import Tarjeta

'''

Implemente la clase Tarjeta que permita manipular (get & set) la siguiente información:
Nombre del titular
Saldo de la tarjeta.
Además, implemente un método que permita actualizar el saldo de la tarjeta producto de una compra.
Considere el uso de un constructor explı́cito.

'''

def nombre_titular():
    ''' 
    permite asignale un nombre a la tarjeta 
    que no sea ""(enter) o " "(espacio)
    '''
    nombre  = str(input("Escriba el nombre del titular: "))
    if nombre is "" or nombre is " ":
        print("no es un nombre valido")
        nombre_titular()
    return nombre

def saldo_inicial():
    '''
    define un saldo inicial de manera aleatoria
    '''
    saldo = random.randint(1,1000)
    print("Usted tiene ${0} de saldo".format(saldo))
    return saldo

def compras():
    valor = int(input("cuanto vale lo que quiere comprar: "))
    return valor

try:
    if __name__ == "__main__":
        print("vienvenido a la marca GuANTE")
        guante = Tarjeta()
        guante.set_nombre_del_titular(nombre_titular())
        #comprobar_nombre(guante.get_nombre_del_titular())
        print("la tarjeta esta a nombre de: ", guante.get_nombre_del_titular())
        guante.set_saldo(saldo_inicial())

        print("\nUsted quiere realizar una compra")
        guante.set_saldo_actual(compras())
    
        print("su saldo actual es:", guante.get_saldo_actual())
except:
    print("\nEn la compra se usaron datos erroneos")