#!/usr/bin/env python3
# -*- coding:utf-8 -*-

class Tarjeta():
    def __init__(self):
        self.__nombre_del_titular = None
        self.__saldo = None

    def set_nombre_del_titular(self, nombre):
        self.__nombre_del_titular = nombre

    def get_nombre_del_titular(self):
        return self.__nombre_del_titular

    def set_saldo(self, saldo):
        self.__saldo = saldo

    def get_saldo(self):
        return self.__saldo

    def set_saldo_actual(self, compra):
        if self.__saldo > 0:
            if self.__saldo >= compra:
                print("Si puede realizar la compra")
                self.__saldo = self.__saldo - compra
            else:
                print("no tiene suficiente saldo")
        else:
            print("no tiene suficiente saldo")

    def get_saldo_actual(self):
        return self.__saldo
