#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import random
from libro import Libro
'''
Crea una clase llamada Libro que guarde la información de cada uno de los libros de una biblioteca.
La clase debe guardar el tı́tulo del libro, autor, número total de ejemplares del libro y número de
ejemplares prestados. La clase contendrá los siguientes métodos:
Constructor con parámetros para la clase libro.
Constructor por defecto para la clase biblioteca.
Métodos Set/Get para los atributos privados

'''

def titulo():
    nombre = str(input("ingrese el titulo del libro: "))
    return nombre

def autor():
    nombre = str(input("ingrese el autor del libro: "))
    return nombre

def ejemplares():
    n = int(input("Cuantos ejemplares va a ingresar de este libro: "))
    return n

def prestados(cantidad_en_biblioteca):
    n = int(input("Cuantos ejmplares va a prestar de este libro: " ))
    if n > cantidad_en_biblioteca:
        print("No hay suficientes libros")
        prestados(cantidad_en_biblioteca)
    return n 

def biblioteca(x):
    '''
    esta funció convierte el identificador ingresado al inicio
    en un objeto de clase Libro
    como algunos atributos son privados, se añade
    el título y el autor por método get
    y llama a las demás funcione spúblicas para ingresar los 
    ejemplares del libro y los prestados 
    '''
    x = Libro()
    x.set_titulo(titulo())
    x.set_autor(autor())
    n = ejemplares()
    x.ejemplares_n(n)
    x.prestados_n(prestados(n))
    return x


def menu():
    '''
    Esta función presenta las opciones de la biblioteca
    si se ingresa un libro tiene l aopción de volver a ingresar otro
    ya que la función es recursiva

    '''
    print("\n---Bienvenido a la biblioteca---")
    print("para ingresar un libro aprete 1")
    print("para salir aprete cualquier letra")
    option = input("--> ") 
    if option == 1:
        x = str(input("ingrese el identificador del libro: "))
        biblioteca(x)
        menu()
    else:
        print("que tenga buen dia")
        #pass

if __name__ == "__main__":
    menu()
    
