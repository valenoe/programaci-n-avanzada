#!/usr/bin/env python3
# -*- coding:utf-8 -*-

class Libro():
    def __init__(self):
        self.__titulo = None
        self.__autor = None
        self.ejemplares = None
        self.prestados = None

    def set_titulo(self, titulo):
        self.__titulo = titulo

    def get_titulo(self):
        return self.__titulo

    def set_autor(self, nombre):
        self.__autor = nombre
    
    def get_autor(self):
        return self.__autor

    def ejemplares_n(self, numero):
        self.ejemplares = numero
        return self.ejemplares

    def prestados_n(self, numero):
        self.prestados = numero
        self.ejemplares = self.ejemplares - self.prestados
        return self.prestados, self.ejemplares
