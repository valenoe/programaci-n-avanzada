

from motor import Motor
from auto import Auto
import random



def encender():
    '''
    se define el caracter con el que se enciende el auto, 
    con cualquier otrom, el vehíclo seguirá apagado
    '''
    cambio = str(input("Aprete espacio para encender "))
    if cambio == " ":
        estado = "encendido"
    else:
        estado = "detenido"
    return estado

def baja_de_combustible(gasolina, estado):
    '''
    función hecha para restar el 1 % de 
    combustible al encender el auto
    '''
    if estado == "encendido":
        gasolina = gasolina - gasolina* 0.01

    print("por encender el auto le quedan {0} Litros".format(gasolina))
    return gasolina

def rueda():
    ruedas = int(input("indique cuantas ruedas va a tener su vehiculo: "))
    return ruedas

def gasto_ruedas(ruedas):
    '''
    función llamada dentro de la función recursiva "avanzar",
    entonces, gasto ruedas se va a producir hasta que el auto deje de avanzar

    por cada movimiento, se produe un gasto de
    entre 1 % y 10 %, definido de manera aleatoria
    esto se descuenta una rueda a la vez,
    --> ruedas - gasto/100
    --> ej: gasto = 5, ruedas = 4 
    --> 4 - 0,05 = 3,95
    así hasta que se detenga el auto

    '''
    gasto = random.randint(1,11)
    gastoRuedas = ruedas - (gasto / 100)
    return gastoRuedas

def velocidad():
    v = int(input("Ingrses a cuantos km/h va a manejar: ")) 
    return v

def gasolinera():
    '''
    define la cantidad de gasolina, como máximo 32 litros,
    de manera aleatoria
    '''
    L = random.randint(1, 33)
    print("su auto tiene {0} Litros".format(L))
    return L

def baja_de_combustible_en_movimiento(combustible, motor, recorrido):
    '''
    Función que reduce la gasolina del estanque segun el 
    recorrido del auto y el cilindraje del motor.

    si cc = 1.2, el motor consume 1 L por cada 20 km
    si cc = 1.6, el motor consume 1 L por cada 14 km
    '''
    if motor == "1.2":
        litro = recorrido / 20
        combustible = combustible - litro

    else:
        litro = recorrido / 14
        combustible = combustible - litro
    return round(combustible, 4)
         
#def gasto_de_combustible_si_no_va_a_v_max(v, d, t, gasolina):

def avanzar(recorrido, combustible, motor, v, ruedas, ruedas_originales):
    '''
    Se establece " "(espacio) para avanzar, si ocupa cualquier otra tecla el auto se detiene
    como estaba detenido el auto, su recorrido = 0
    La distancia que recoore está definida por velocidad * tiempo
    y el tiempo de manera aleatoria.
    se llama inicialmente a motor para poder llevarlo a otra función
    que calcule el gasto de gasolina.
    Esta función es recursiva, así que va a estar corriendo hasta que se acabe la gasolina



    '''

    cambio = str(input("\nAprete espacio para avanzar "))
    if cambio == " ":
        tiempo = (random.randint(1,11)) / 360
        recorrido = recorrido + (v * tiempo)
        
        print("ha avanzado: {0} kilometros".format(recorrido))
        
        gasolina = baja_de_combustible_en_movimiento(combustible, motor, recorrido) 
        gasto_ruedas(ruedas)    
        if gasto_ruedas == ruedas_originales - 1:
            print("se revento una rueda")
            pass
        if gasolina > 0:

            print("quedan {0} L".format(gasolina))
            avanzar(recorrido, gasolina, motor, v, ruedas, ruedas_originales)
        else:
            print('no hay combustible')

    else:
        print("No apretó el acelerador\nse va a a detener el auto")


try:
    
    if __name__ == "__main__":
        # crea objetos
        motor = Motor()
        c = Auto()


        #agrega ruedas
        c.set_rueda(rueda())


        # define la gasolina con la que empieza el auto
        c.set_Estanque(gasolinera())
    

        # encende el auto
        estado = encender()
        c.Encendio_Apagado(estado)


 
    
        if estado == "encendido":


            # si el auto esta encendido, baja la gasolina
            gasolina = baja_de_combustible(c.get_Estanque(), estado)


            # se establece la velocidad a la que va a andar 
            c.set_velocimetro(velocidad())
        
        
    
            # define la nueva gasolina 
            c.set_Estanque(gasolina)
        


            # si va a velocidad máxima
            print("el motor tiene una cilindrada de: ", motor.cilindrada)


            # Funcion para avanzar
            ruedas_originales =  c.get_rueda()
            avanzar(0, c.get_Estanque(), motor.cilindrada, c.get_velocimetro(), c.get_rueda(), ruedas_originales)
        
        else:
            print("el auto no se va a mover")

except:
    print("apreto algo que no corresponde")
