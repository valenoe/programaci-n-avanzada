#!/usr/bin/env python3
# -*- coding:utf-8 -*-



class Auto():

    # atributos

    def __init__(self):
        self.__Estanque = None
        self.__rueda = None
        self.__velocimetro = None

    # métodos
    
    def set_Estanque(self, Estanque):
        self.__Estanque = Estanque

    def get_Estanque(self):
        return self.__Estanque

    def set_rueda(self, ruedas):
        self.__rueda = ruedas
    
    def get_rueda(self):
        return self.__rueda

    def set_velocimetro(self, velocimetro):
        self.__velocimetro = velocimetro
        
    def get_velocimetro(self):
        return self.__velocimetro

    def Encendio_Apagado(self, estado):
        self.estado = estado

    def se_mueve(self, avanzar):
        self.avanzar = avanzar

